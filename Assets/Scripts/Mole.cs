﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mole : MonoBehaviour
{
    //Variables

    public bool IsPoppin = false;

    public Animator Anim;

    public BoxCollider Collider;

    public void InvokePop()
    {
        IsPoppin = true;

        StopAllCoroutines();

        StartCoroutine(Poppin());
    }

    IEnumerator Poppin()
    {
        Anim.Play("MolePop");

        //Collider.enabled = true;

        yield return null;

        while (Anim.GetCurrentAnimatorStateInfo(0).IsName("MolePop"))
        {
            IsPoppin = true;

            yield return null;
        }

        IsPoppin = false;
    }

    public void InvokeHit()
    {
        if (!Anim.GetCurrentAnimatorStateInfo(0).IsName("MolePop"))
        {
            GameManager.GM.ReduceLife(1);

            return;
        }

        GameManager.GM.AddScore(1);
    }
}
