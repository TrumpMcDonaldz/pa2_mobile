﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class _HighScore
{
    public double SecondsHS { get; set; }

    public DateTime DT { get; set; }
}
