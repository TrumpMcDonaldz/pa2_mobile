﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class WinLose : MonoBehaviour
{
    public Text WinLoseText;

    public void SetWinLose(bool Win, TimeSpan TS)
    {
        if (Win)
        {
            WinLoseText.text = $"VICTORY ROYALE! {((HighScore.IsHighscore(TS)) ? $"New HighScore: {TS.TotalSeconds} s!" : "")}";
        }

        else
        {
            WinLoseText.text = "LOSER ROYALE!";
        }

        Time.timeScale = 0;
    }

    public void Restart()
    {
        print("Restart");

        SceneManager.LoadScene("Game");
    }

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }
}
