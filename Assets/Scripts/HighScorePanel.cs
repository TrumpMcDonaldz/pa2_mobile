﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScorePanel : MonoBehaviour
{
    public Text HighScoreText;

    void Awake()
    {
        var GetHS = HighScore.GetHighScore();

        HighScoreText.text = (GetHS == default) ? "No Highscore Available!" : $"{GetHS.SecondsHS} Seconds!";
    }
}
