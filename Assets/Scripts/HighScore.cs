﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters.Binary;

public class HighScore : MonoBehaviour
{
    public static string ThePath = $"{Path.GetDirectoryName(Application.persistentDataPath)}/HighScore.dat";

    static BinaryFormatter bf = new BinaryFormatter();

    public static bool IsHighscore(TimeSpan TimeTaken)
    {
        if (!File.Exists(ThePath))
        {
            FileStream file = File.Open(ThePath, FileMode.Create);

            bf.Serialize(file, new _HighScore { SecondsHS = TimeTaken.TotalSeconds, DT = DateTime.UtcNow });

            file.Close();

            return true;
        }

        FileStream file1 = File.Open(ThePath, FileMode.Open);

        var CurrentHighScore = (_HighScore)bf.Deserialize(file1);

        file1.Close();

        if (TimeTaken.TotalSeconds < CurrentHighScore.SecondsHS)
        {
            FileStream file = File.Open(ThePath, FileMode.Create);

            bf.Serialize(file, new _HighScore { SecondsHS = TimeTaken.TotalSeconds, DT = DateTime.UtcNow });

            file.Close();

            return true;
        }

        return false;
    }

    public static _HighScore GetHighScore()
    {
        if (!File.Exists(ThePath))
        {
            return default;
        }

        FileStream file1 = File.Open(ThePath, FileMode.Open);

        var CurrentHighScore = (_HighScore)bf.Deserialize(file1);

        file1.Close();

        return CurrentHighScore;
    }
}
