﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Raycaster : MonoBehaviour
{
    private void Update()
    {
        if (Input.touches == default || Input.touches.Count() == 0)
        {
            print("nOt TouchING");

            return;
        }


        var Touch = Input.touches.First();

        if (Touch.phase != TouchPhase.Began)
        {
            return;
        }

        //var Ray = Camera.main.ScreenPointToRay(Touch.position);

        var Pos = Camera.main.ScreenToWorldPoint(Touch.position);

        var Hit = Physics2D.Raycast(Pos, Pos, Mathf.Infinity);

        if (Hit == default)
        {
            return;
        }
        var Mole = Hit.collider.GetComponent<Mole>();

        if (Mole == default)
        {
            return;
        }

        print("TOuching");

        Mole.InvokeHit();
    }
}
