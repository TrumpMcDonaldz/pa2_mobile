﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    //Variables

    public static GameManager GM;

    public int Score, Life;

    public List<Mole> Moles;

    public Text ScoreText, LifeText;

    public WinLose WinLose;

    public DateTime StartTime;

    public Raycaster RC;

    private void Awake()
    {
        StartTime = DateTime.UtcNow;

        Time.timeScale = 1;

        AddScore(0);

        ReduceLife(0);

        GM = this;

        Moles = GameObject.FindObjectsOfType<Mole>().ToList();

        StartCoroutine(PopManager());
    }

    IEnumerator PopManager()
    {
        while (Moles.Any(x => x.IsPoppin))
        {
            yield return null;
        }

        yield return new WaitForSecondsRealtime(UnityEngine.Random.Range(1, 3));

        Moles[UnityEngine.Random.Range(0, Moles.Count() - 1)].InvokePop();

        StartCoroutine(PopManager());
    }

    public void AddScore (int ScoreToAdd)
    {
        Score += ScoreToAdd;

        ScoreText.text = $"Score: {Score}";

        if (Score >= 20)
        {
            Destroy(RC.gameObject);

            //Win

            var TS = DateTime.UtcNow - StartTime;

            WinLose.gameObject.SetActive(true);

            WinLose.SetWinLose(true, TS);
        }
    }

    public void ReduceLife(int LifeToReduce)
    {
        Life -= LifeToReduce;

        LifeText.text = $"Life: {Life}";

        if (Life <= 0)
        {
            Destroy(RC.gameObject);

            //Lose

            WinLose.gameObject.SetActive(true);

            WinLose.SetWinLose(false, default);
        }
    }
}
